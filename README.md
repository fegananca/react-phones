# The App

This is a web page that retrieves and display a list of mobile phones from a JSON file.

The app consists in two pages:

1. Product List: a list of phones with infinite scroll tool.
2. Product Detail: display detail information about a selected phone.

The app was built using React and Sass.


## Getting started

In the project directory, you should run:

### `npm install`
Install all dependencies.

### `npm start`
Runs the app in development mode.
Open [http://localhost:3000] to view in your browser.

### `npm run build`
Runs the app in production mode.