import { fireEvent, render, screen} from '@testing-library/react';
import { MemoryRouter as Router, Route, Routes } from 'react-router';
// import { createMemoryHistory } from 'history';
import  ProductList  from './ProductList';
import  ProductDetail  from './ProductDetail';
import mockData from '../services/mockSingleData.json';
import * as api from '../services/api'


jest.mock('../services/api', () => ({
    getProductsList: jest.fn(),
    getSingleProduct: jest.fn(),
  }));

describe('Product detail page', () => {
  beforeEach(() => jest.clearAllMocks());


  test('should navigate to product detail page when "Learn More" button is clicked', async () => {

    api.getProductsList.mockResolvedValue(mockData);
    api.getSingleProduct.mockResolvedValue(mockData);
     render(
      <Router>
        <Routes>
          <Route exact path="/" element={<ProductList/>} />
          <Route exact path="/products/:id" element={<ProductDetail/>} />
        </Routes>
      </Router>
    );
  
    const learnMoreButton = await screen.findByText('Learn More');
    fireEvent.click(learnMoreButton);
    await screen.findByText('Quad-core 1.3 GHz Cortex-A53');

    expect(await screen.findByText('Add') ).toBeDisabled()
    
      });
});