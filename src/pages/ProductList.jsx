import React, { useEffect, useState } from "react";
import Header from "../components/Header";
import SearchBar from "../components/SearchBar";
import { getProductsList } from "../services/api";
import {
  getLocalDataPhones,
  getPhoneExpiration,
  removeLocalDataPhones,
  removePhoneExpiration,
  storeExpirationTime,
  storePhones,
} from "../services/localStorage";

const ProductList = ({ count }) => {
  const [phones, setPhones] = useState([]);

  const fetchPhones = async () => {
    const localData = getLocalDataPhones();
    const phoneExpiration = getPhoneExpiration();

    if (phoneExpiration && new Date().getTime() > phoneExpiration) {
      removeLocalDataPhones();
      removePhoneExpiration();
    }

    if (localData) {
      setPhones(localData.slice(0, 8));
    } else {
      const expirationTime = new Date().getTime() + 60 * 60 * 1000;
      const data = await getProductsList();
      storePhones(data);
      storeExpirationTime(expirationTime);
      setPhones(data.slice(0, 8));
    }
  };

  useEffect(() => {
    fetchPhones();
  }, []);

  return (
    <>
      <Header count={count}></Header>
      <SearchBar phones={phones} setPhones={setPhones}></SearchBar>
    </>
  );
};

export default ProductList;
