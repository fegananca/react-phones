import {  render, screen, waitFor } from '@testing-library/react';
import { MemoryRouter as Router } from 'react-router-dom';
import  ProductList  from './ProductList';
import mockData from '../services/mockSingleData.json';
import * as api from '../services/api'

jest.mock('../services/api', () => ({
    getProductsList: jest.fn(),
    getSingleProduct: jest.fn(),
  }));

describe('Products list', () => {
  beforeEach(() => jest.clearAllMocks());

  test('render products', async () => {
    api.getProductsList.mockResolvedValue(mockData);
    render(
      <Router>
        <ProductList/>
      </Router>
    );
    await waitFor(() => {
      screen.getByText('Acer');
    });
  });


  test('navigation on click', async () => {
    api.getProductsList.mockResolvedValue(mockData);
    render(
      <Router>
        <ProductList />
      </Router>
    );

    await waitFor(() => {
    expect(screen.getByText('Learn More')).toHaveAttribute('href', '/products/ZmGrkLRPXOTpxsU4jjAcv');
    });
  });

});