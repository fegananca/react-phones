import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import CardDetail from "../components/CardDetail";
import Header from "../components/Header";
import { getSingleProduct } from "../services/api";

const ProductDetail = ({ count, setCount }) => {
  const [productInfo, setProductInfo] = useState("");

  const location = useLocation();
  const selectedProduct = location.state?.data;

  useEffect(() => {
    const fetchProductId = async () => {
      const data = await getSingleProduct();
      const findProduct = data.find((product) => {
        return product.id === selectedProduct.id;
      });

      setProductInfo(findProduct);
    };
    fetchProductId();
  }, [selectedProduct]);

  return (
    <>
      <Header count={count}></Header>
      <CardDetail productInfo={productInfo} setCount={setCount}></CardDetail>
    </>
  );
};

export default ProductDetail;
