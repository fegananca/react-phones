export const getLocalDataPhones = () => {JSON.parse(localStorage.getItem("phones"))};

export const getPhoneExpiration = () => {JSON.parse(
  localStorage.getItem("phonesExpiration")
)};

export const getCartItems = () => {JSON.parse(localStorage.getItem("cart"))};

export const removeLocalDataPhones = () => {localStorage.removeItem("phones")};

export const removePhoneExpiration = () => {localStorage.removeItem("phonesExpiration")};

export const storePhones =  (data) => {localStorage.setItem("phones", JSON.stringify(data))};

export const storeExpirationTime =  (data) => {localStorage.setItem("phonesExpiration", JSON.stringify(data))};

export const storeCartExpirationTime = (data) => {localStorage.setItem("cartExpiration", JSON.stringify(data))};
export const storeCartItems = (data) => {localStorage.setItem("cart", JSON.stringify(data))};