import { allProducts } from "./db/allProducts.fixture";
import { singleProduct } from "./db/singleProduct.fixture";

export const getProductsList = async () => {
    return allProducts;
};

export const getSingleProduct = async () => {
    return singleProduct;
};
