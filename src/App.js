import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import  ProductList  from "./pages/ProductList";
import ProductDetail from "./pages/ProductDetail";
import { useState } from "react";

function App() {
  const cartItems = JSON.parse(localStorage.getItem("cart")) || [];
  const cartExpiration = localStorage.getItem("cartExpiration");
  if (cartExpiration && new Date().getTime() > cartExpiration) {
    localStorage.removeItem("cart");
    localStorage.removeItem("cartExpiration");
  }

  const [count, setCount] = useState(cartItems ? cartItems.length : 0);

  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<ProductList count={count} />}></Route>
          <Route
            path="/products/:id"
            element={<ProductDetail count={count} setCount={setCount} />}
          ></Route>
          <Route path="/products"></Route>;
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
