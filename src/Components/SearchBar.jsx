import React, { useState } from "react";
import "../styles/SearchBar.scss";
import InfiniteScroll from "react-infinite-scroll-component";
import { CardData } from "./CardData";

const SearchBar = ({ phones, setPhones }) => {
  const [searchQuery, setSearchQuery] = useState("");
  const [hasMore, setHasMore] = useState(true);

  const fetchMoreData = () => {
    const localStorageData = JSON.parse(localStorage.getItem("phones"));
    const nextData = localStorageData.slice(phones.length, phones.length + 8);
    setPhones([...phones, ...nextData]);

    if (phones.length >= localStorageData.length) {
      setHasMore(false);
    }
  };

  const onSearchData = (e) => {
    setSearchQuery(e.target.value);
  };

  const filteredData = searchQuery
    ? phones.filter((item) => {
        return (
          item.brand.toLowerCase().includes(searchQuery.toLowerCase()) ||
          item.model.toLowerCase().includes(searchQuery.toLowerCase()) ||
          item.price.includes(searchQuery)
        );
      })
    : phones;

  return (
    <div className="main">
      <input type="text" placeholder="Search for" onChange={onSearchData} />

      <InfiniteScroll
        dataLength={phones.length}
        next={fetchMoreData}
        hasMore={hasMore}
        loader={filteredData.length > 0 && <h4>Loading...</h4>}
        endMessage={
          filteredData.length > 0 && (
            <p style={{ textAlign: "center" }}>
              <b>Yay! You've seen it all!</b>
            </p>
          )
        }
      >
        {filteredData.length > 0 ? (
          <div className="container">
            {filteredData.map((phone) => {
              return <CardData phone={phone} key={phone.id}></CardData>;
            })}
          </div>
        ) : (
          <p id="message">No results found</p>
        )}
      </InfiniteScroll>
    </div>
  );
};

export default SearchBar;
