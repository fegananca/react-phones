import React from "react";
import "../styles/BreadCrumbs.scss";
import { Link, useLocation } from "react-router-dom";

export const BreadCrumbs = () => {
  const location = useLocation();

  return (
    <nav>
      <Link to="/" className="breadcrumbs-active">
        Home
      </Link>
      <span className="breadcrumbs-arrow">&gt;</span>
      <Link
        to={`/`}
        className={
          location.pathname.startsWith("/products")
            ? "breadcrumbs-active"
            : "breacrumbs-inactive"
        }
      >
        Brands
      </Link>
      <span className="breadcrumbs-arrow">&gt;</span>
      <Link
        to={`/products/${location.state?.data.id}`}
        className={
          location.pathname === `{/products/${location.state?.data.id}}`
            ? "breadcrumbs-active"
            : "breadcrumbs-inactive"
        }
      >
        {location.state?.data.brand}
      </Link>
    </nav>
  );
};
