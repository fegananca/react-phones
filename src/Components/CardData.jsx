import React from "react";
import "../styles/CardData.scss";
import { Link } from "react-router-dom";
import image from "../assets/rodion-kutsaiev-0VGG7cqTwCo-unsplash.jpg";

export const CardData = ({ phone }) => {
  return (
    <div className="card">
      <h2>{phone.brand}</h2>
      <h3>{phone.model}</h3>
      <figure>
        <img src={image} alt="phone card" />
      </figure>
      <div className="card-info">
        <p>€{phone.price}</p>
        <Link
          to={`/products/${phone.id}`}
          state={{ data: phone }}
          className="button"
          id="learn-more"
        >
          Learn More
        </Link>
      </div>
    </div>
  );
};
