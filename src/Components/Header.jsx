import React from "react";
import "../styles/Header.scss";
import { Icon } from "@iconify/react";
import { Link } from "react-router-dom";

const Header = ({ count }) => {
  return (
    <header>
      <div className="container">
        <h2>
          <Link to="/">
            My<span className="logo">App</span>
          </Link>
        </h2>
        <div className="cart">
          <Icon
            icon="ph:bag-simple-thin"
            width={40}
            height={40}
            color="aliceblue"
          />
          <span data-testid="cart-count">{count}</span>
        </div>
      </div>
    </header>
  );
};

export default Header;
