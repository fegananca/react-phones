import React, { useState } from "react";
import "../styles/CardDetail.scss";
import image from "../assets/rodion-kutsaiev-0VGG7cqTwCo-unsplash.jpg";
import { BreadCrumbs } from "./BreadCrumbs";
import {
  storeCartExpirationTime,
  storeCartItems,
} from "../services/localStorage";

const CardDetail = ({ productInfo, setCount }) => {
  const [selectedOptions, setSelectedOptions] = useState({
    colors: { name: null, code: null },
    storages: { name: null, code: null },
  });

  const handleColorClick = (name, code) => {
    setSelectedOptions({
      ...selectedOptions,
      colors: { name: name, code: code },
    });
  };

  const handleStorageClick = (name, code) => {
    setSelectedOptions({
      ...selectedOptions,
      storages: { name: name, code: code },
    });
  };

  const onAdd = (e) => {
    e.preventDefault();
    if (
      selectedOptions.colors.code !== null &&
      selectedOptions.storages.code !== null
    ) {
      const cartItems = JSON.parse(localStorage.getItem("cart")) || [];
      const updatedCartItems = [...cartItems, selectedOptions];
      const expirationTime = new Date().getTime() + 60 * 60 * 1000;
      storeCartExpirationTime(expirationTime);
      storeCartItems(updatedCartItems);
    }
  };

  const handleAddClick = () => {
    const localData = JSON.parse(localStorage.getItem("cart"));
    setCount(localData.length);
  };

  return (
    <>
      <BreadCrumbs></BreadCrumbs>
      <div className="container">
        <figure>
          <img id="img-box" src={image} alt="phone card" />
        </figure>
        <div className="card" id="card-detail">
          <div className="description">
            <h2>{productInfo.brand}</h2>
            <h3>{productInfo.model}</h3>
            <p id="price">€{productInfo.price}</p>
            <p>{productInfo.cpu}</p>
            <p>{productInfo.ram}</p>
            <p>{productInfo.os}</p>
            <p>{productInfo.displayResolution}</p>
            <p>{productInfo.battery}</p>
            <p>Primary camera: {productInfo.primaryCamera}</p>
            <p>Secondary camera: {productInfo.secondaryCmera}</p>
            <p>{productInfo.dimentions}</p>
            <p>Weight: {productInfo.weight}g</p>
          </div>
          <form onSubmit={onAdd}>
            <span>Available in:</span>
            <div className="container-btns">
              {productInfo &&
                productInfo.options.colors.map((color) => {
                  return (
                    <button
                      className="color-btns"
                      style={{ backgroundColor: color.name, color: color.name }}
                      onClick={() => handleColorClick(color.name, color.code)}
                      key={color.code}
                    >
                      {color.name}
                    </button>
                  );
                })}
            </div>
            <p>{selectedOptions.colors.name}</p>
            <div className="container-btns">
              {productInfo &&
                productInfo.options.storages.map((storage) => {
                  return (
                    <div key={storage.code}>
                      <button
                        className="storage-btns"
                        onClick={() =>
                          handleStorageClick(storage.name, storage.code)
                        }
                      >
                        {storage.name}
                      </button>
                    </div>
                  );
                })}
            </div>
            <p>{selectedOptions.storages.name}</p>
            <button
              className="button"
              id="add"
              type="button"
              disabled={
                selectedOptions.colors.code === null ||
                selectedOptions.storages.code === null
              }
              onClick={handleAddClick}
            >
              Add
            </button>
          </form>
        </div>
      </div>
    </>
  );
};

export default CardDetail;
